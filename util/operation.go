package util

func ValueExistInArray(value string, array []string) bool {
	for _, eachResult := range array {
		if eachResult == value {
			return true
		}
	}
	return false
}
