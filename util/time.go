package util

import (
	"fmt"
	"time"
)

func DateToEpochTime(date string) int64 {
	local, _ := time.LoadLocation("Asia/Chongqing")
	date += " 00:00:00"
	layout := "2006-01-02 15:04:05"
	t, err := time.ParseInLocation(layout, date, local)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println(t.Unix() * 1000)
	return t.Unix() * 1000
}

func FullDateToEpochTime(date string) int64 {
	local, _ := time.LoadLocation("Asia/Chongqing")
	// 2017-01-01T00:00:00.000Z
	ymd := date[0:10]
	hms := date[11:19]
	date = ymd + "T" + hms + ".000Z"
	layout := "2006-01-02T15:04:05.000Z"
	t, err := time.ParseInLocation(layout, date, local)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println(t.Unix() * 1000)
	return t.Unix() * 1000
}

func EpochTimeToDate(etime int64) string {
	tm := time.Unix(etime/1000, 0)
	// fmt.Println(reflect.TypeOf(timeInteger))
	return tm.Format(time.RFC3339)[0:19]
}

func EpochTimeToDateDay(etime int64) string {
	chinaLocal, _ := time.LoadLocation("Asia/Chongqing")
	tm := time.Unix(etime/1000, 0)
	// fmt.Println(reflect.TypeOf(timeInteger))
	return tm.In(chinaLocal).Format(time.RFC3339)[0:10]
}

func GetCurrentDay() string {
	chinaLocal, _ := time.LoadLocation("Asia/Chongqing")
	tmp := time.Now()
	return tmp.In(chinaLocal).Format(time.RFC3339)[0:10]
}

func GetYesterday() string {
	chinaLocal, _ := time.LoadLocation("Asia/Chongqing")
	tmp := time.Now().AddDate(0, 0, -1)
	return tmp.In(chinaLocal).Format(time.RFC3339)[0:10]
}

func GetTomorrowDay() string {
	chinaLocal, _ := time.LoadLocation("Asia/Chongqing")
	tmp := time.Now().AddDate(0, 0, 1)
	return tmp.In(chinaLocal).Format(time.RFC3339)[0:10]
}

func GetDeadline(date string) string {
	local, _ := time.LoadLocation("Asia/Chongqing")

	date += "T00:00:00.000Z"
	layout := "2006-01-02T15:04:05.000Z"
	t, err := time.Parse(layout, date)
	if err != nil {
		fmt.Println(err)
	}
	tmp := t.AddDate(0, 0, 1)
	return tmp.In(local).Format(time.RFC3339)[0:10]
}
