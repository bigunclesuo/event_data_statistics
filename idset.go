package statistics

import (
	"sync"

	"github.com/bigunclesuo/event_data_statistics/util"

	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetIdSet(num int, fromSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()
	collection := fromSessionCopy.DB(fromDatabase).C("event_data")
	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	log.Println(startTime, endTime)
	var idList []string
	err := collection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime}}).Distinct("id", &idList)
	log.Println("numbers of ids: ", len(idList))
	log.Println(idList)

	for i := 0; i < num; i++ {
		idListChan <- idList
	}
	if err != nil {
		log.Println(err)
	}
	waitgroup.Done()
}
