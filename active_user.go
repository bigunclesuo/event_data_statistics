package statistics

import (
	"log"
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func ActiveTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()

	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin ActiveTest")

	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	// fmt.Println(startTime, endTime)

	dataResult := make(map[string]interface{})
	resultMap := make(map[string]db.ActiveUserSchema)

	toActiveUserCollection := toSessionCopy.DB(toDatabase).C("activeUsers")
	idList := <-idListChan
	for _, v := range idList {
		condition := []bson.M{{"type": "navigation"}, {"type": "media"},
			{"type": "voiceassistant"}, {"type": "bluetooth"}, {"type": "wifihotspot"}, {"type": "clickarea"}}
		eachResult := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v, "$or": condition}).Sort("timeUnix").Iter()
		for eachResult.Next(&dataResult) {
			emitTime := dataResult["timeUnix"].(int64)

			id := dataResult["id"].(string)
			project := dataResult["project"].(string)
			date := util.EpochTimeToDate(emitTime)[0:10]
			// fmt.Println(emitTime, date)
			find := false
			for k, _ := range resultMap {
				if k == id {
					find = true
				}
			}

			if !find {
				var rs = db.ActiveUserSchema{Project: project, Id: id, Active_time: emitTime, Inactive_time: emitTime, Date: date}
				resultMap[id] = rs
			} else {
				v := resultMap[id]
				if emitTime-v.Active_time >= 1000*60*60 {
					if v.Active_time != v.Inactive_time {
						err := toActiveUserCollection.Insert(v)
						if err != nil {
							log.Println(err)
						}
					}

					var rs = resultMap[id]
					rs.Active_time = emitTime
					rs.Inactive_time = emitTime
					resultMap[id] = rs
				} else {
					var rs = resultMap[id]
					rs.Inactive_time = emitTime
					resultMap[id] = rs
				}
			}
		}
	}

	for _, v := range resultMap {
		if v.Inactive_time != v.Active_time {
			err := toActiveUserCollection.Insert(v)
			if err != nil {
				log.Println(err)
			}
		}
	}

	log.Println("finish ActiveTest")
	waitgroup.Done()
}
