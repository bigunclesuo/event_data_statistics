package statistics

import (
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func NavigationTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()

	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin NavigationTest")

	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	toNavigationCollection := toSessionCopy.DB(toDatabase).C("navigation")

	idList := <-idListChan
	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	condition := []bson.M{{"type": "navigation"}, {"properties.acc": false, "event": "acc"}}

	for _, v := range idList {
		eachResult := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v, "$or": condition}).Sort("timeUnix").Iter()
		var activeTime int64 = 0

		var dataResult = make(map[string]interface{})
		for eachResult.Next(&dataResult) {
			var navi string
			var acc bool
			emitTime := dataResult["timeUnix"].(int64)
			id := dataResult["id"].(string)
			project := dataResult["project"].(string)
			properties := dataResult["properties"].(map[string]interface{})
			if properties["navigation"] != nil {
				navi = properties["navigation"].(string)
			}

			if properties["acc"] != nil {
				acc = properties["acc"].(bool)
			}
			date := util.EpochTimeToDate(emitTime)[0:10]

			if navi == "true" {
				activeTime = emitTime
			} else if navi == "false" || acc == false {
				if activeTime != 0 {
					tmp := db.NavigationSchema{Id: id, Project: project, Active_time: activeTime,
						Inactive_time: emitTime, Date: date}
					err := toNavigationCollection.Insert(tmp)
					if err != nil {
						log.Println(err)
					}
				}
				activeTime = 0
			}

		}
	}
	log.Println("finish NavigationTest")
	waitgroup.Done()
}
