package db

type DataSchema struct {
	Project    string                 `bson:"project" json:"project"`
	Time       uint64                 `bson:"_time" json:"_time"`
	Properties map[string]interface{} `bson:"properties" json:"properties"`
	Type       string                 `bson:"type" json:"type"`
	Event      string                 `bson:"event" json:"event"`
}

type UserSchema struct {
	Id          string
	Project     string
	Create_time int64
	Date        string
}

type OnlineUserSchema struct {
	Id           string
	Project      string
	Online_time  int64
	Offline_time int64
	Date         string
}

type ActiveUserSchema struct {
	Project       string
	Id            string
	Active_time   int64
	Inactive_time int64
	Date          string
}

type MusicSchema struct {
	Id            string
	Project       string
	Music_id      string
	Active_time   int64
	Inactive_time int64
	Date          string
}

type PodcastSchema struct {
	Id            string
	Project       string
	Podcast_id    string
	Active_time   int64
	Inactive_time int64
	Date          string
}

type MapSchema struct {
	Id            string
	Project       string
	Active_time   int64
	Inactive_time int64
	Date          string
}

type NavigationSchema struct {
	Id            string
	Project       string
	Active_time   int64
	Inactive_time int64
	Date          string
}

type StayPointSchema struct {
	Id        string
	Project   string
	Stay_time int64
	Move_time int64
	Loc       []float64
	Date      string
}

type WakeupSchema struct {
	Id          string
	Project     string
	Date        string
	Active_time int64
	Word        string
}

type InteractionSchema struct {
	Id            string
	Project       string
	Date          string
	Active_time   int64
	Inactive_time int64
	Origin_text   string
	Score         string
	Semantic      string
}
