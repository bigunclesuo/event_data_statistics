package db

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math"

	"gopkg.in/mgo.v2"
)

func ReadJsonFile(filePath string) (map[string]interface{}, error) {
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println("readConfig:", err.Error())
		return nil, err
	}

	var content = make(map[string]interface{})
	if err := json.Unmarshal(bytes, &content); err != nil {
		fmt.Println("Unmarshal: ", err.Error())
		return nil, err
	}
	return content, nil
}

func CreateSession(url string) (*mgo.Session, error) {
	if url == "" {
		return nil, errors.New("invalid parameters")
	}
	session, err := mgo.Dial(url)
	if err != nil {
		return nil, err
	}
	session.SetMode(mgo.Monotonic, true)
	return session, nil
}

func CloseSession(session *mgo.Session) {
	if session != nil {
		(*session).Close()
	}
}

func GetCollection(session *mgo.Session, database string, collectionName string) (*mgo.Collection, error) {
	if session == nil || database == "" || collectionName == "" {
		return nil, errors.New("invalid parameters")
	}
	return session.DB(database).C(collectionName), nil
}

type Page struct {
	start int
	size  int
}

func SplitPages(pageSize int, count int) []Page {
	fmt.Println("prepare page size:", pageSize, "all count", count)

	var ret []Page
	allPage := int(math.Ceil(float64(count) / float64(pageSize)))
	fmt.Println("count:", count, " need ", allPage, " pages")
	if pageSize >= count {
		ret = append(ret, Page{0, count})
	} else {
		for i := 0; i < allPage-1; i++ {
			ret = append(ret, Page{i * pageSize, pageSize})
		}
		ret = append(ret, Page{(allPage - 1) * pageSize, count - (allPage-1)*pageSize})
	}
	return ret
}

// 109010
//func Test() {
//	config, err := ReadJsonFile("./config/config.json")
//	if err != nil {
//		panic(err)
//	}
//
//	session, err := CreateSession(config["url"])
//	if err != nil {
//		panic(err)
//	}
//
//	collection, err := GetCollection(session, config["database"], "event_data")
//	if err != nil {
//		panic(err)
//	}
//
//	v, _ := mem.VirtualMemory()
//	fmt.Printf("Total: %v, Free:%v, UsedPercent:%f%%\n", v.Total, v.Free, v.UsedPercent)
//
//	var res []DataSchema
//
//	//fmt.Println("---skip---")
//	//t := time.Now().UnixNano()/1e6
//	//collection.Find(bson.M{"id": "cyn_44"}).Sort("time").Skip(109005).Limit(5).All(&res)
//	//fmt.Println("result:", res[0].Time)
//	//fmt.Println(time.Now().UnixNano()/1e6 - t)
//	//
//	fmt.Println("---find---")
//	t1 := time.Now().UnixNano() / 1e6
//
//	var mongoPages MongoPages
//	count, _ := collection.Find(bson.M{"id": "cyn_44"}).Count()
//	pages := SplitPages(10000, count)
//
//	for i := 0; i < len(pages); i++ {
//		mongoPages.ReadDatas(collection, pages[i].start, pages[i].size, &res)
//	}
//
//	//collection.Find(bson.M{"id": "cyn_44", "time": bson.M{"$gte": 1486718621319}}).Sort("time").Limit(5).All(&res)
//
//	fmt.Println("find spend time:", time.Now().UnixNano()/1e6-t1, "ms")
//
//	fmt.Println("---iter---")
//	var data DataSchema
//	t2 := time.Now().UnixNano() / 1e6
//	var ccc int32
//
//	var datas []DataSchema // buffer
//
//	iter := collection.Find(bson.M{"id": "cyn_44"}).Iter() //  buffer
//	for iter.Next(&data) {
//		ccc = ccc + 1
//		datas = append(datas, data)
//		if len(datas)%1000 == 0 {
//			// go work
//		}
//	}
//	fmt.Println(ccc)
//
//	if err := iter.Close(); err != nil {
//		fmt.Println(err.Error())
//	}
//	fmt.Println("iter spend time:", time.Now().UnixNano()/1e6-t2, "ms")
//
//	//fmt.Printf("Total: %v, Free:%v, UsedPercent:%f%%\n", v.Total, v.Free, v.UsedPercent)
//
//	// all
//	//t := time.Now().UnixNano()/1e6
//	//var res []DataSchema
//	//err = collection.Find(bson.M{"id": "cyn_44"}).All(&res)
//	//if err != nil {
//	//	log.Fatal(err)
//	//}
//	//fmt.Println("result:", len(res))
//	//fmt.Println(time.Now().UnixNano()/1e6 - t)
//
//	//fmt.Println("result.$acc", result.Properties["$acc"])
//	//fmt.Println("result.$address", result.Properties["$address"])
//	//fmt.Println("result.$total_size", result.Properties["$total_size"])
//
//	CloseSession(session)
//}
