package db

import (
	"fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type MongoPages struct {
	last int
	time uint64
}

// start uint32, count uint32, sort string,
func (self *MongoPages) ReadDatas(collection *mgo.Collection, start int, size int, res *[]DataSchema) {
	if start == self.last {
		collection.Find(bson.M{"id": "cyn_44", "time": bson.M{"$gt": self.time}}).Sort("time").Limit(size).All(res)
		fmt.Println("find:[", start, "-", size, "]")
	} else {
		collection.Find(bson.M{"id": "cyn_44"}).Sort("time").Skip(start).Limit(size).All(res)
		fmt.Println("skip:[", start, "-", size, "]")
	}
	self.last = start + size
	self.time = (*res)[size-1].Time
}
