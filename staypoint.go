package statistics

import (
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"log"
	"reflect"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func StayPointTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()

	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin StayPointTest")

	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	toStayPointCollection := toSessionCopy.DB(toDatabase).C("stayPoint")
	idList := <-idListChan

	dataResult := make(map[string]interface{})
	var emptyLoc []float64
	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))

	for _, v := range idList {
		eachResult := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v, "event": "gpsupdate"}).Sort("timeUnix").Iter()

		var concernPoint = db.StayPointSchema{Loc: emptyLoc}
		for eachResult.Next(&dataResult) {
			emitTime := dataResult["timeUnix"].(int64)
			id := dataResult["id"].(string)
			project := dataResult["project"].(string)
			properties := dataResult["properties"].(map[string]interface{})
			date := dataResult["time"].(string)[0:10]
			loc := reflect.ValueOf(properties["loc"])
			var lat float64
			var lng float64
			if loc.Interface() != nil {
				typeOfLat := reflect.TypeOf(loc.Index(1).Interface()).Kind()
				if typeOfLat == reflect.Float64 {
					lat = loc.Index(1).Interface().(float64)
				}

				if typeOfLat == reflect.Int {
					lat = float64(loc.Index(1).Interface().(int))
				}

				typeOfLng := reflect.TypeOf(loc.Index(0).Interface()).Kind()
				if typeOfLng == reflect.Float64 {
					lng = loc.Index(0).Interface().(float64)
				}

				if typeOfLng == reflect.Int {
					lng = float64(loc.Index(0).Interface().(int))
				}
			}

			var speed float64
			speed = -1
			if properties["speed"] != nil {
				if reflect.TypeOf(properties["speed"]).Kind() == reflect.Float64 {
					speed = properties["speed"].(float64)
				}

				if reflect.TypeOf(properties["speed"]).Kind() == reflect.Int {
					speed = float64(properties["speed"].(int))
				}

			}

			if speed == 0 {
				// fmt.Println("speed 0")
				if concernPoint.Id == "" {
					// fmt.Println("concernPoint.Id == empty")

					concernPoint = db.StayPointSchema{Id: id, Project: project, Stay_time: emitTime, Loc: emptyLoc, Date: date}
				}
			} else if speed >= 10 {
				if concernPoint.Id != "" {
					if emitTime-concernPoint.Stay_time > 5000*60*5 {
						stayTime := concernPoint.Stay_time
						loc := []float64{lng, lat}
						tmp := db.StayPointSchema{Id: id, Project: project, Stay_time: stayTime,
							Move_time: emitTime, Loc: loc, Date: date}
						err := toStayPointCollection.Insert(tmp)
						if err != nil {
							log.Println(err)
						}
						concernPoint = db.StayPointSchema{Loc: emptyLoc}
					}
				}
			}
		}
	}
	log.Println("finish StayPointTest")
	waitgroup.Done()
}
