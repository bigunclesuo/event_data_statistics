package statistics

import (
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func OnlineTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()
	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin OnlineTest")
	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	toOnlineUserCollection := toSessionCopy.DB(toDatabase).C("onlineUsers")

	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	dataResult := make(map[string]interface{})
	resultMap := make(map[string]db.OnlineUserSchema)
	idList := <-idListChan

	for _, v := range idList {
		eachResult := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v, "event": "gpsupdate"}).Sort("timeUnix").Iter()
		for eachResult.Next(&dataResult) {
			emitTime := dataResult["timeUnix"].(int64)
			id := dataResult["id"].(string)
			project := dataResult["project"].(string)
			date := util.EpochTimeToDate(emitTime)[0:10]
			find := false
			for k, _ := range resultMap {
				if k == id {
					find = true
				}
			}

			if !find {
				var rs = db.OnlineUserSchema{Id: id, Project: project, Online_time: emitTime,
					Offline_time: emitTime, Date: date}
				resultMap[id] = rs
			} else {
				v := resultMap[id]
				if emitTime-v.Offline_time >= 1000*60*60 {
					if v.Online_time != v.Offline_time {
						onlineData := db.OnlineUserSchema{Id: id, Project: project, Online_time: v.Online_time,
							Offline_time: v.Offline_time, Date: date}
						toOnlineUserCollection.Insert(onlineData)
					}

					var rs = resultMap[id]
					rs.Online_time = emitTime
					rs.Offline_time = emitTime
					resultMap[id] = rs
				} else {
					var rs = resultMap[id]
					rs.Offline_time = emitTime
					resultMap[id] = rs
				}
			}
		}
		eachResult.Close()
	}

	for _, v := range resultMap {
		if v.Offline_time != v.Online_time {
			onlineData := db.OnlineUserSchema{Project: v.Project, Id: v.Id,
				Online_time: v.Online_time, Offline_time: v.Offline_time, Date: v.Date}
			err := toOnlineUserCollection.Insert(onlineData)
			if err != nil {
				log.Println(err)
			}
		}
	}

	log.Println("finish OnlineTest")
	waitgroup.Done()
}
