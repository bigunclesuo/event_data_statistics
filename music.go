package statistics

import (
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func MusicTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()

	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin MusicTest")

	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	toMusicCollection := toSessionCopy.DB(toDatabase).C("music")
	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	idList := <-idListChan
	for _, v := range idList {
		eachResult := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v, "type": "media", "event": "music"}).Sort("timeUnix").Iter()
		var musicIdMap = make(map[string]int64)
		var dataResult = make(map[string]interface{})
		for eachResult.Next(&dataResult) {
			if dataResult["type"] != "media" {
				continue
			}
			if dataResult["event"] != "music" {
				continue
			}

			emitTime := dataResult["timeUnix"].(int64)
			id := dataResult["id"].(string)
			project := dataResult["project"].(string)
			properties := dataResult["properties"].(map[string]interface{})
			musicId := properties["music_id"].(string)
			state := properties["state"].(string)
			date := util.EpochTimeToDate(emitTime)[0:10]
			if state == "play" {
				musicIdMap[musicId] = emitTime
				musicIdMap[musicId] = emitTime
			}
			if state == "stop" || state == "pause" || state == "complete" {
				duration := emitTime - musicIdMap[musicId]
				if musicIdMap[musicId] != 0 && 600000 > duration && duration > 5000 {
					tmp := db.MusicSchema{Id: id, Project: project, Music_id: musicId,
						Active_time: musicIdMap[musicId], Inactive_time: emitTime, Date: date}
					err := toMusicCollection.Insert(tmp)
					if err != nil {
						log.Println(err)
					}
				}
				musicIdMap[musicId] = 0
			}
		}
		eachResult.Close()
	}
	log.Println("finish MusicTest")
	waitgroup.Done()
}
