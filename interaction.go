package statistics

import (
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func InteractionTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()

	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin InteractionTest")

	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	toInteractionCollection := toSessionCopy.DB(toDatabase).C("interaction")
	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	idList := <-idListChan
	condition := []bson.M{{"event": "recognizer"}, {"event": "interaction"}}
	dataResult := make(map[string]interface{})

	for _, v := range idList {

		eachResult := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v, "type": "voiceassistant", "$or": condition}).Sort("timeUnix").Iter()
		var activeTime int64
		activeTime = 0
		allText := ""
		score := ""
		semantic := ""
		for eachResult.Next(&dataResult) {
			var interaction bool
			var originText string
			var scoreText string
			var semanticText string
			emitTime := dataResult["timeUnix"].(int64)
			id := dataResult["id"].(string)
			project := dataResult["project"].(string)
			event := dataResult["event"].(string)
			properties := dataResult["properties"].(map[string]interface{})
			if properties["interaction"] != nil {
				interaction = properties["interaction"].(bool)
			}

			if properties["origin_text"] != nil {
				originText = properties["origin_text"].(string)
			}

			if properties["score"] != nil {
				scoreText = properties["score"].(string)
			}

			if properties["semantic"] != nil {
				semanticText = properties["semantic"].(string)
			}

			date := util.EpochTimeToDate(emitTime)[0:10]

			if event == "interaction" && interaction == true {
				activeTime = emitTime
			} else if event == "recognizer" {
				allText += originText + ","
				score += scoreText + ","
				semantic += semanticText + ","
			} else if event == "interaction" && interaction == false && properties["interaction"] != nil {
				if len(allText) >= 2 && len(score) >= 2 && len(semantic) >= 2 {
					tmp := db.InteractionSchema{Id: id, Project: project, Date: date, Active_time: activeTime,
						Inactive_time: emitTime, Origin_text: allText[0 : len(allText)-1],
						Score: score[0 : len(score)-1], Semantic: semantic[0 : len(semantic)-1]}
					err := toInteractionCollection.Insert(tmp)
					if err != nil {
						log.Println(err)
					}
				}
				activeTime = 0
				allText = ""
				score = ""
				semantic = ""
			}
		}
	}
	log.Println("finish InteractionTest")
	waitgroup.Done()
}
