package statistics

import (
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// PodcastTest calculate podcast
func PodcastTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()

	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin PodcatTest")
	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	toPodcastCollection := toSessionCopy.DB(toDatabase).C("podcast")

	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	idList := <-idListChan

	for _, v := range idList {
		eachResult := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v, "type": "media", "event": "podcast"}).Sort("timeUnix").Iter()
		PodcastidMap := make(map[string]int64)
		dataResult := make(map[string]interface{})
		for eachResult.Next(&dataResult) {
			emitTime := dataResult["timeUnix"].(int64)
			id := dataResult["id"].(string)
			project := dataResult["project"].(string)
			properties := dataResult["properties"].(map[string]interface{})
			Podcastid := properties["podcast_id"].(string)
			state := properties["state"].(string)
			date := util.EpochTimeToDate(emitTime)[0:10]
			if state == "play" {
				PodcastidMap[Podcastid] = emitTime
			}

			if state == "stop" || state == "pause" || state == "complete" {
				duration := emitTime - PodcastidMap[Podcastid]
				if PodcastidMap[Podcastid] != 0 && duration > 5000 && duration < 3600000 {
					tmp := db.PodcastSchema{Id: id, Project: project, Podcast_id: Podcastid,
						Active_time: PodcastidMap[Podcastid], Inactive_time: emitTime, Date: date}
					err := toPodcastCollection.Insert(tmp)
					if err != nil {
						log.Println(err)
					}
					PodcastidMap[Podcastid] = 0
				}
			}
		}
	}
	log.Println("finish PodcastTest")
	waitgroup.Done()
}
