package statistics

import (
	"fmt"
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func WakeupTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()

	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin WakeupTest")

	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	toWakeupCollection := toSessionCopy.DB(toDatabase).C("wakeup")
	idList := <-idListChan
	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	dataResult := make(map[string]interface{})

	for _, v := range idList {
		eachResult := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v, "type": "voiceassistant", "event": "wakeup"}).Sort("timeUnix").Iter()
		for eachResult.Next(&dataResult) {
			emitTime := dataResult["timeUnix"].(int64)
			id := dataResult["id"].(string)
			project := dataResult["project"].(string)
			properties := dataResult["properties"].(map[string]interface{})
			word := properties["word"].(string)
			date := util.EpochTimeToDate(emitTime)[0:10]
			tmp := db.WakeupSchema{Id: id, Project: project, Date: date, Active_time: emitTime, Word: word}
			err := toWakeupCollection.Insert(tmp)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
	log.Println("finish WakeupTest")
	waitgroup.Done()
}
