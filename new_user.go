package statistics

import (
	"log"
	"sync"

	"github.com/bigunclesuo/event_data_statistics/db"
	"github.com/bigunclesuo/event_data_statistics/util"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func NewTest(fromSession *mgo.Session, toSession *mgo.Session, config map[string]string, waitgroup *sync.WaitGroup, idListChan chan []string) {
	fromDatabase := config["fromDatabase"]
	date := config["calculateDate"]
	toDatabase := config["toDatabase"]

	fromSessionCopy := fromSession.Copy()
	defer fromSessionCopy.Close()

	toSessionCopy := toSession.Copy()
	defer toSessionCopy.Close()

	log.Println("Begin NewTest")

	fromEventDataCollection := fromSessionCopy.DB(fromDatabase).C("event_data")
	toHistoryUserCollection := toSessionCopy.DB(toDatabase).C("users")

	var historyIDList []string
	err := toHistoryUserCollection.Find(bson.M{}).Distinct("id", &historyIDList)
	if err != nil {
		log.Println(err)
	}

	idMapTime := make(map[string]db.UserSchema)
	emptyUserCase := db.UserSchema{Id: "", Project: "", Create_time: 0, Date: ""}

	startTime := util.DateToEpochTime(date)
	endTime := util.DateToEpochTime(util.GetDeadline(date))
	idList := <-idListChan
	for _, v := range idList {
		var result map[string]interface{}
		err := fromEventDataCollection.Find(bson.M{"timeUnix": bson.M{"$gt": startTime, "$lt": endTime},
			"id": v}).Sort("timeUnix").One(&result)
		if err != nil {
			log.Println(err)
		}

		if result == nil {
			continue
		}

		id := result["id"].(string)
		project := result["project"].(string)
		time := result["timeUnix"].(int64)
		date := util.EpochTimeToDate(result["timeUnix"].(int64))

		if !util.ValueExistInArray(id, historyIDList) {
			if idMapTime[id] != emptyUserCase {
				if idMapTime[id].Create_time > time {
					var tmp = idMapTime[id]
					tmp.Create_time = time
					idMapTime[id] = tmp
				}
			} else {
				var tmp = db.UserSchema{Id: id, Project: project, Create_time: time, Date: date}
				idMapTime[id] = tmp
			}
		}
	}

	for k, v := range idMapTime {
		data := db.UserSchema{Id: k, Project: v.Project, Create_time: v.Create_time, Date: v.Date}
		err := toHistoryUserCollection.Insert(data)
		if err != nil {
			log.Println(err)
		}
		// log.Println("insert history user")
	}
	log.Println("finish NewTest")
	waitgroup.Done()
}
